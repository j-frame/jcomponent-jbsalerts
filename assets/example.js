// Bootstrap ScrollSpy
function initBsScrollSpy(targetSelector,offsetHeight) {
    targetSelector = typeof targetSelector !== 'undefined' ? targetSelector : '#navbar';
    offsetHeight   = typeof offsetHeight !== 'undefined'   ? offsetHeight   : 0;

    $('body').scrollspy({
        target: targetSelector,
        offset: offsetHeight
    });

    // Scroll To
    $(targetSelector).find('a').click(function () {
        var scrollTarget = $(this).attr('href');
        var scrollTargetOffset = $(scrollTarget).offset();
        var scrollPos = scrollTargetOffset.top - offsetHeight;
        $('body, html').animate({
            scrollTop: scrollPos
        }, 500);
        return false;
    });
}

jQuery(function($) {
    $('body').tooltip({
        selector: '[data-toggle="tooltip"]',
        container: 'body',
        viewport: 'body'
    });

    // Bootstrap ScrollSpy
    initBsScrollSpy('#navbar', 70);
    
    // Example Action
    $('.exampleAddAlertButton').click( function() {
    	var thisType = '';
    	if($(this).hasClass('btn-info')) {
    		thisType = 'info';
    	}
    	if($(this).hasClass('btn-success')) {
    		thisType = 'success';
    	}
    	if($(this).hasClass('btn-warning')) {
    		thisType = 'warning';
    	}
    	if($(this).hasClass('btn-danger')) {
    		thisType = 'danger';
    	}
	    var example_type      = thisType;
	    var example_message   = $('.example-options #message').val();
	    var example_position  = $('.example-options #position').val();
	    var example_addClass  = $('.example-options #addClass').val();
	    var example_autoClose = $('.example-options #autoClose').prop('checked');
	    var example_acDelay   = $('.example-options #acDelay').val();
	    var example_acSpeed   = $('.example-options #acSpeed').val();
	    var example_toast     = $('.example-options #toast').prop('checked');
    	
    	var jsBsAlerts_example_options = {};

        if(example_type !== '') {
            jsBsAlerts_example_options.type = example_type;
        }
        if(example_message === '') {
        	example_message = 'Test';
        }
        jsBsAlerts_example_options.message = example_message;
        
        if(example_position !== '') {
            jsBsAlerts_example_options.position = example_position;
        }
        if(example_addClass !== '') {
            jsBsAlerts_example_options.addClass = example_addClass;
        }
        jsBsAlerts_example_options.autoClose = example_autoClose;
        if(example_acDelay !== '' && example_acDelay === parseInt(example_acDelay, 10)) {
            jsBsAlerts_example_options.acDelay = example_acDelay;
        }
        if(example_acSpeed !== '') {
            jsBsAlerts_example_options.acSpeed = example_acSpeed;
        }
        jsBsAlerts_example_options.toast = example_toast;
        
        
        console.log('jsBsAlerts_example_options',jsBsAlerts_example_options);
        
        $('#alertsHere').jBsAlerts(jsBsAlerts_example_options);
        
    });
});